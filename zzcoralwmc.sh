git pull

rm wmcpng/*

jupyter nbconvert --to html --execute --ExecutePreprocessor.timeout=3600 zzcorwmc.ipynb
jupyter nbconvert --to html --execute --ExecutePreprocessor.timeout=3600 zzcorwmcr.ipynb

rm zzcorwmc.html
rm zzcorwmcr.html

cat zzcorhead.html zzcoradwmcr.html zzcormid.html wmcr.html zzcortail.html > zzcoralwmc.html

git add .
git commit -m "Update & push from zzcoralwmc.sh"
git push
